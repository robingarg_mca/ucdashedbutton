//
//  AbstractTitleCardView.h
//  LEGO Superheroes
//
//  Created by Shawn Bernard on 1/6/12.
//  Copyright (c) 2012 Uncorked Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AbstractTitleCardView : UIView {

    UIButton *editButton;
}

@property (nonatomic, retain) UIButton *editButton;


@end
