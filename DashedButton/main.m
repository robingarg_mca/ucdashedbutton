//
//  main.m
//  DashedButton
//
//  Created by Shawn Bernard on 1/24/12.
//  Copyright (c) 2012 Uncorked Studios. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
